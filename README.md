Front End - HTML, CSS, JS
Back End - NodeJs (Express - REST api)

This project includes the following
    - REST api with session handling using Express_Session framework
    - Automated mail service for forget password option using nodemailer SMTP protocol
    
