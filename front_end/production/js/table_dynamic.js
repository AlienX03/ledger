var rowCounter=1;
var credits=0;
var debits=0;
var total=0;
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json",function (data) {
    host=data.host;

})

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getUrlParam(parameter, defaultvalue){
    var urlparameter = defaultvalue;

    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}



function addTableRows(id,Date,Entity,Description,Debit,Credit,PaymentMode){
    var row = "<tr>";
    row+="<td id ='"+id+"'>"+rowCounter+"</td>";
    row+="<td>"+Date+"</td>";
    row+="<td>"+Entity+"</td>";
    row+="<td>"+Description+"</td>";
    row+="<td>"+PaymentMode+"</td>";
    row+="<td>"+Debit+"</td>";
    row+="<td>"+Credit+"</td>";
    row+="</tr>";
    credits+=parseInt(Credit);
    debits+=parseInt(Debit);
    total+=(Credit-Debit);

    $('#Credits').empty();
    $('#Credits').append(credits);
    $('#Debits').empty();
    $('#Debits').append(debits);
    var totalElement= document.getElementById('Total');
    if(total==0){
        totalElement.style.color="grey";
    }
    else if(total>0){
        totalElement.style.color="green";
    }
    else{
        totalElement.style.color="red";
    }
    $('#Total').empty();
    $('#Total').append(total);
    $('#transactionTable').append(row);

    rowCounter++;
}

var BusinessId = getUrlParam("id",-1);
if(BusinessId==-1){
    BusinessId = sessionStorage.getItem("businessId");
    if(sessionStorage.getItem("businessId")===null){
        alert("Sorry, no Records found");
    }
    else{
        initialConfiguration();
    }


}
else{
    initialConfiguration();
}

function initialConfiguration(){
    var request = new XMLHttpRequest();
    var data = {};
    data.businessId = BusinessId;
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            // Typical action to be performed when the document is ready:

            var clients = JSON.parse(this.response);


            for(var key in clients){
                var transactions = clients[key].transactions;
                for(var transactionKey in transactions){
                    if(transactions[transactionKey].transactionmode.includes("credit")){
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,clients[key].name,transactions[transactionKey].description,0,transactions[transactionKey].amount,transactions[transactionKey].paymentmode);
                    }
                    else{
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,clients[key].name,transactions[transactionKey].description,transactions[transactionKey].amount,0,transactions[transactionKey].paymentmode);
                    }


                }
            }
            document.getElementById("businessName").innerText=clients.businessName;
            sessionStorage.setItem("businessName",clients.businessName);
            $("#clientTransactions").DataTable({
                dom:"Blfrtip",
                buttons:[
                    {extend:"copy",className:"btn-sm"},
                    {extend:"csv",className:"btn-sm"},
                    {extend:"excel",className:"btn-sm"},
                    {
                        text: 'Pdf',
                        className:"btn-sm",
                        extend: 'pdfHtml5',
                        filename: clients.businessName,
                        orientation: 'portrait', //portrait
                        pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                        exportOptions: {
                            columns: ':visible',
                            search: 'applied',
                            order: 'applied'
                        },
                        customize: function (doc) {
                            var now = new Date();
                            var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                            doc.content[1].table.widths =
                                Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                            doc['header']=(function() {
                                return {
                                    columns: [
                                        {
                                            alignment: 'left',
                                            fontSize: 14,
                                            text: 'Business Name: '+clients.businessName
                                        },
                                        {
                                            alignment: 'right',
                                            fontSize: 14,
                                            text: 'Date: '+jsDate
                                        }
                                    ],
                                    margin: 20
                                }
                            });
                        }
                    },
                    {extend:"print",className:"btn-sm"}]
            });




        }
    };
    request.open("POST",host+"/fetchClients",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(data));

}
var businessName = sessionStorage.getItem("businessName");
$("#headerTitle")[0].innerText= businessName;

sessionStorage.setItem("businessId",BusinessId);
