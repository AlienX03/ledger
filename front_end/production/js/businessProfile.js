var prevContent;
var host="";
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json",function (data) {
    host=data.host;

});
initialConfiguration();
function initialConfiguration() {
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            var jsonData = JSON.parse(this.response);
            for(itr of jsonData){

                addCards(itr.id,itr.name,itr.address,itr.mobile,itr.mail,itr.tax,itr.ie);
            }
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
        }
    };
    request.open("GET",host+"/fetchBusiness",true);
    request.send();

}




function addCards(id,title,address,phNo,mail,tax,ie){
    var content = '<div class="row cards">';
    content+='<div class="x_panel">';
    content+='<div class="x_title" id="title'+id+'">';
    content+='<button type="button" style="background: transparent;border: none !important;" onclick="viewBusiness('+id+')"><h2>'+title+'</h2></button>';
    content+='</div>';
    content+='<div class="x_content" style="display: none" ><div class="col-md-2 col-sm-3" style="font-size: medium;font-weight: bold;">Address </div><div class="col-md-1 col-sm-1">:</div> <div class="col-md-6 col-sm-6" id="address'+id+'"> '+address+'</div></div>';
    content+='<div class="x_content" style="display: none" ><div class="col-md-2 col-sm-3" style="font-size: medium;font-weight: bold">Mobile </div><div class="col-md-1 col-sm-1">:</div><div class="col-md-6 col-sm-6" id="phoNO'+id+'">'+phNo+'</div></div>';
    content+='<div class="x_content" style="display: none" ><div class="col-md-2 col-sm-3" style="font-size: medium;font-weight: bold">E-mail </div><div class="col-md-1 col-sm-1">:</div><div class="col-md-6 col-sm-6" id="email'+id+'">'+mail+'</div></div>';
    content+='<div class="x_content" style="display: none"><div class="col-md-2 col-sm-3" style="font-size: medium;font-weight: bold">Tax </div><div class="col-md-1 col-sm-1">:</div><div class="col-md-6 col-sm-6" id="tax'+id+'">'+tax+'</div></div>';
    content+='<div class="x_content" style="display: none" ><div class="col-md-2 col-sm-3" style="font-size: medium;font-weight: bold">IE code </div><div class="col-md-1 col-sm-1">:</div><div class="col-md-6 col-sm-6" id="ie'+id+'">'+ie+'</div></div>';
    content+='<div class="x_content" id="buttonContent'+id+'">';
    content+='<div class="row">';
    content+='<div class="col-md-6 col-sm-6"></div>';
    content+='<div class="col-md-6 col-sm-6">';
    content+='<button class="btn btn-md pull-right btn-primary" data-placement="right" onclick="viewBusiness('+id+')"><span class="glyphicon glyphicon-eye-open"> </span> View';
    content+='</button>';
    content+='<button class="btn btn-md pull-right btn-danger" data-placement="right"  onclick="deleteBusiness('+id+')" ><span class="glyphicon glyphicon-trash"> </span> Delete';
    content+='</button>';
    content+='<button class="btn btn-md pull-right btn-info" data-placement="right" onclick="edit('+id+')"><span class="glyphicon glyphicon-pencil"> </span> Edit';
    content+='</button>';
    content+='</div></div></div></div></div>';
    $('#BusinessBlocks').append(content);
}

function deleteBusiness(id){
    $('.bs-delete-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-danger" onclick="deleteConfirmation('+id+')">Delete</button>';
    $('deleteButton').empty();
    $('deleteButton').append(button);
}
function  deleteConfirmation(id) {

    var username = document.getElementById("username").value;
    var password = document.getElementById("pass").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                var deleteConfirmation = prompt("Please enter CONFIRM to delete");
                if(deleteConfirmation.includes("CONFIRM")){
                    var request = new XMLHttpRequest();
                    request.withCredentials=true;

                    request.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            // Typical action to be performed when the document is ready:
                            if(this.response.includes("success")){
                                location.reload();
                            }
                            else{
                                alert("sorry, try again");
                            }
                            if(this.response.includes("LoggedOut")){
                                window.location.href="index.html";
                            }
                        }
                    };
                    request.open("POST",host+"/deleteBusiness",true);
                    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    var idData = {};
                    idData.id=id;
                    request.send(JSON.stringify(idData));
                }


            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}
function editingEnable(id){
    var prevTitle = $('#title'+id)[0].innerText;
    var prevAddress = $('#address'+id)[0].innerText;
    var prevMobile = $('#phoNO'+id)[0].innerText;
    var prevEmail = $('#email'+id)[0].innerText;
    var prevTax = $('#tax'+id)[0].innerText;
    var prevIe = $('#ie'+id)[0].innerText;
    $('#newUser')[0].innerText = "Edit user";
    document.getElementById('businessName').value=prevTitle;
    document.getElementById('address').value=prevAddress;
    document.getElementById('phNO').value=prevMobile;
    document.getElementById('mail').value=prevEmail;
    document.getElementById('taxId').value=prevTax;
    document.getElementById('ieCode').value=prevIe;
    var button = '<button type="button" class="btn btn-primary" onclick="update('+id+')">Save Changes</button>';
    $('editButton').empty();
    $('editButton').append(button);
}

function edit(id){
    $('.bs-edit-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-primary" onclick="editConfirmation('+id+')">Confirm</button>';
    $('editConfirmationButton').empty();
    $('editConfirmationButton').append(button);
}

function editConfirmation(id){

    var username = document.getElementById("user").value;
    var password = document.getElementById("password").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                $('.bs-edit-modal-sm').modal('toggle')
                $('.bs-editUser-modal-sm').modal('toggle');
                editingEnable(id);
            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}


function createUser(){

    var postData = {};
    
    postData.name = document.getElementById('newBusinessName').value;
    postData.address = document.getElementById('newAddress').value;
    postData.mobile = document.getElementById('newPhNO').value;
    postData.mail = document.getElementById('newMail').value;
    postData.tax = document.getElementById('newTaxId').value;
    postData.ie =  document.getElementById('newIeCode').value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                location.reload();
            }
            else{
                alert("sorry, try again");
            }

        }
    };
    request.open("POST",host+"/addBusiness",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(postData));
}
function update(id){
    var postData = {};
    postData.id = id;
    postData.name = document.getElementById('businessName').value;
    postData.address = document.getElementById('address').value;
    postData.mobile = document.getElementById('phNO').value;
    postData.mail = document.getElementById('mail').value;
    postData.tax = document.getElementById('taxId').value;
    postData.ie =  document.getElementById('ieCode').value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
           if(this.response.includes("success")){
               location.reload();
           }
           else{
               alert("sorry, try again");
           }
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
        }
    };
    request.open("POST",host+"/updateBusiness",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(postData));
}

function search(){
    var searchKey = document.getElementById("searchElement").value;
    var elements = document.getElementsByClassName("cards");

    for(element of elements){
        var tempSearchElement = element.getElementsByTagName("div")[1].innerText;
        if(tempSearchElement.includes(searchKey)){
            element.getElementsByTagName("div")[0].style.display="";
        }
        else{
            element.getElementsByTagName("div")[0].style.display="none";
        }

    }
    //console.log(elements[0].getElementsByTagName("div")[1].innerText);
}


function viewBusiness(id){
    window.location.href="tables_dynamic.html?id="+id;
}