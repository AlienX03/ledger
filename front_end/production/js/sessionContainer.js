
var host="";
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json",function (data) {
    host=data.host;

})
function logOut(){
    var request = new XMLHttpRequest();
    //request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:

            if(this.response.includes("logging out")){
                window.location.href="index.html";
            }
        }
    };
    request.open("GET",host+"/logout",true);
    request.send();

}
function sessionCheck() {
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            console.log(this.response);
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
        }
    };
    request.open("GET",host+"/fetchSession",true);
    request.send()
}
sessionCheck();