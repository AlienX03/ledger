function loadScript(src){
    var script = document.createElement('script');
    script.src = src;
    script.async = false;
    document.head.appendChild(script);
}

var host="";
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json",function (data) {
    host=data.host;

})

var request = new XMLHttpRequest();
request.withCredentials = true;
request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        // Typical action to be performed when the document is ready:

    }
};
request.open("GET",host+"/logout",true);
request.send();


function loginFunction(){
    var username = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("success")){
                window.location.href = 'businessProfile.html';



            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));

}
