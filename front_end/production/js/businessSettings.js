var BusinessId = sessionStorage.getItem("businessId");
var businessName = sessionStorage.getItem("businessName");
$("#headerTitle")[0].innerText= businessName;

if (sessionStorage.getItem("businessId") === null) {
    alert("Sorry, no Records found");
} else {
    initialConfiguration()
}
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json", function (data) {
    host = data.host;

});


function  updateBusinessSettings(name,address,email,mobile,tax,ie) {
    $('#BusinessName').append(name);
    $('#Address').append(address);
    $('#Email').append(email);
    $('#mobile').append(mobile);
    $('#tax').append(tax);
    $('#ie').append(ie);
}

function initialConfiguration(){
    var button='<button type="button" class="btn btn-danger" id="editBusiness" onclick="edit('+BusinessId+')"><span class="glyphicon glyphicon-pencil"></span>  Edit</button>';
    $('editButton').empty();
    $('editButton').append(button);
    var request = new XMLHttpRequest();
    var data = {};
    data.businessId = BusinessId;
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            var profile = JSON.parse(this.response)[0];
            updateBusinessSettings(profile.name,profile.address,profile.mail,profile.mobile,profile.tax,profile.ie);
            console.log(profile);



        }
    };
    request.open("POST",host+"/fetchBusinessSettings",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(data));
}

function edit(id){
    $('.bs-edit-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-primary" onclick="editConfirmation('+id+')">Confirm</button>';
    $('editConfirmationButton').empty();
    $('editConfirmationButton').append(button);
}

function editConfirmation(id){

    var username = document.getElementById("user").value;
    var password = document.getElementById("password").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                $('.bs-edit-modal-sm').modal('toggle')
                $('.bs-editUser-modal-sm').modal('toggle');
                editingEnable(id);
            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}
function editingEnable(id){
    var prevTitle = $('#BusinessName')[0].innerText;
    var prevAddress = $('#Address')[0].innerText;
    var prevMobile = $('#mobile')[0].innerText;
    var prevEmail = $('#Email')[0].innerText;
    var prevTax = $('#tax')[0].innerText;
    var prevIe = $('#ie')[0].innerText;
    $('#newUser')[0].innerText = "Edit user";
    document.getElementById('businessName1').value=prevTitle;
    document.getElementById('address').value=prevAddress;
    document.getElementById('phNO').value=prevMobile;
    document.getElementById('mail').value=prevEmail;
    document.getElementById('taxId').value=prevTax;
    document.getElementById('ieCode').value=prevIe;
    var button = '<button type="button" class="btn btn-primary" onclick="update('+id+')">Save Changes</button>';
    $('editButton').empty();
    $('editButton').append(button);
}

function update(id){
    var postData = {};
    postData.id = id;
    postData.name = document.getElementById('businessName1').value;
    postData.address = document.getElementById('address').value;
    postData.mobile = document.getElementById('phNO').value;
    postData.mail = document.getElementById('mail').value;
    postData.tax = document.getElementById('taxId').value;
    postData.ie =  document.getElementById('ieCode').value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                location.reload();
            }
            else{
                alert("sorry, try again");
            }
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
        }
    };
    request.open("POST",host+"/updateBusiness",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(postData));
}
