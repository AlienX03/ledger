var businessName = sessionStorage.getItem("businessName");
$("#headerTitle")[0].innerText= businessName;

var rowCounter=1;
var credits=0;
var debits=0;
var total=0;
var balance=0;
var balanceCredit=0;
var balanceDebit=0;
var transactions=[];
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json",function (data) {
    host=data.host;

})
var BusinessId = sessionStorage.getItem("businessId");
if (sessionStorage.getItem("businessId") === null) {
    alert("Sorry, no Records found");
} else {
    initialConfiguration()
}
function iterateTransaction(id) {

    for (var itr = 0; itr < transactions.length; itr++) {

        if (transactions[itr].id == id) {
            return transactions[itr];
        }
    }
    return "Not found";
}


Date.prototype.getWeek = function (dowOffset) {
    /*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.meanfreepath.com */

    dowOffset = typeof(dowOffset) == 'int' ? dowOffset : 0; //default dowOffset to zero
    var newYear = new Date(this.getFullYear(),0,1);
    var day = newYear.getDay() - dowOffset; //the day of week the year begins on
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() -
        (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
    var weeknum;
    //if the year starts before the middle of a week
    if(day < 4) {
        weeknum = Math.floor((daynum+day-1)/7) + 1;
        if(weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1,0,1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum+day-1)/7);
    }
    return weeknum;
};
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getUrlParam(parameter, defaultvalue){
    var urlparameter = defaultvalue;

    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}

function verify(){
    if(document.getElementById("from").value!="" && document.getElementById("paymentMode").value!=0 && document.getElementById("bankName").value!=0 && document.getElementById("description").value!="" && document.getElementById("remarks").value!="" && document.getElementById("amount").value!="" && (document.getElementById("credit").checked || document.getElementById("debit").checked)){
        return "true";
    }
    else{
        return "false";
    }
}


function addTransaction(){
    var response = verify();



    if(response.includes("true")){
        var button = document.getElementById("addTransaction");

        button.disabled=true;

        var sendData = {};
        var date=document.getElementById("from").value;
        var dateArr = date.split(".");
        var inputDate = new Date(parseInt(dateArr[2]),parseInt(dateArr[1])-1,parseInt(dateArr[0]));
        sendData.week=inputDate.getWeek();
        sendData.year=dateArr[2];
        sendData.month=dateArr[1];
        sendData.date=date;
        sendData.clientId=clientId;
        sendData.balance=document.getElementById("amount").value;
        sendData.paymentMode = document.getElementById("paymentMode").value;
        sendData.bank = document.getElementById("bankName").value;
        sendData.description = document.getElementById("description").value;
        sendData.remarks=document.getElementById("remarks").value;
        if(document.getElementById("credit").checked){
            sendData.mode="credit";
        }
        else{
            sendData.mode="debit";
        }
        var clientRequest = new XMLHttpRequest();
        clientRequest.withCredentials=true;
        clientRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                // Typical action to be performed when the document is ready:
                if(this.response.includes("success")){
                    location.reload();
                }
                else{
                    alert("sorry! try again");
                    button.disabled=false;
                }
            }
        };
        clientRequest.open("POST",host+"/addClientTransaction",true);
        clientRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        clientRequest.send(JSON.stringify(sendData));

    }
    else{
        //alert("Please fill all the particulars");
    }
}

var clientId = getUrlParam("id",-1);
if(clientId==-1){
    clientId = sessionStorage.getItem("clientId");
    if(sessionStorage.getItem("clientId")===null){
        alert("Sorry, no Records found");
    }
    else{

    }


}
else{

}

function edit(id){
    $('.bs-edit-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-primary" id="editConfirmation" onclick="editConfirmation('+id+')">Confirm</button>';
    $('editConfirmationButton').empty();
    $('editConfirmationButton').append(button);
}
function editConfirmation(id){
    var button = document.getElementById("editConfirmation");
    button.disabled=true;
    var username = document.getElementById("user").value;
    var password = document.getElementById("password").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("success")){
                $('.bs-edit-modal-sm').modal('toggle')
                $('.bs-editTransaction-modal-sm').modal('toggle');
                button.disabled=false;
                editUser(id);
            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}
function editUser(id){
    var transaction = iterateTransaction(id);
    document.getElementById("date").value=transaction.date;
    document.getElementById("editPaymentMode").value=transaction.paymentMode;
    document.getElementById("editBankName").value=transaction.bank;
    document.getElementById("editDescription").value=transaction.description;
    document.getElementById("editRemarks").value=transaction.remarks;
    var amount=0;
    var mode="";
    if(transaction.credit!=0){
        amount=transaction.credit;
        mode="credit";
    }
    else{
        amount=Math.abs(transaction.debit);
        mode="debit";
    }
    document.getElementById("editAmount").value=amount;
    if(mode.includes("credit")){
        $("#editCredit").prop("checked",true);
    }
    else{
        $("#editDebit").prop("checked",true);
    }
    var button = '<button type="button" class="btn btn-primary" id="update" onclick="update(' + id + ')">Update</button>';
    $('editButton').empty();
    $('editButton').append(button);
}
function  editVerify() {
    if(document.getElementById("date").value!="" && document.getElementById("editPaymentMode").value!="" && document.getElementById("editDescription").value!="" && document.getElementById("editRemarks").value!="" && document.getElementById("editAmount").value!="" && (document.getElementById("editCredit").checked || document.getElementById("editDebit").checked)){
        return true;
    }
    else{
        return false;
    }
}
function update(id) {
    var button = document.getElementById("update");
    button.disable=true;
    var sendData = {};
    var check = editVerify();
    if(check){
        sendData.date=document.getElementById("date").value;
        sendData.paymentMode=document.getElementById("editPaymentMode").value;
        sendData.bank=document.getElementById("editBankName").value
        sendData.description=document.getElementById("editDescription").value;
        sendData.remarks=document.getElementById("editRemarks").value;
        sendData.balance=document.getElementById("editAmount").value;
        if(document.getElementById("editCredit").checked){
            sendData.mode="credit";
        }
        else{
            sendData.mode="debit";
        }
        sendData.id=id;
        var dateArr = sendData.date.split(".");
        var inputDate = new Date(parseInt(dateArr[2]),parseInt(dateArr[1])-1,parseInt(dateArr[0]));
        sendData.week=inputDate.getWeek();
        sendData.year=dateArr[2];
        sendData.month=dateArr[1];
        sendData.clientId=clientId;
        var clientRequest = new XMLHttpRequest();
        clientRequest.withCredentials=true;
        clientRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if(this.response.includes("success")){
                    window.location.href="clientTransaction.html?id="+clientId;
                }
            }

        };
        clientRequest.open("POST",host+"/editClientTransaction",true);
        clientRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        clientRequest.send(JSON.stringify(sendData));


    }
    else{
        alert("Please fill all the particulars");
    }

}
function deleteClient(id){
    $('.bs-delete-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-danger" onclick="deleteConfirmation('+id+')">Delete</button>';
    $('deleteButton').empty();
    $('deleteButton').append(button);
}
function  deleteConfirmation(id) {

    var username = document.getElementById("username").value;
    var password = document.getElementById("pass").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("success")){
                var deleteConfirmation = prompt("Please enter CONFIRM to delete");
                if(deleteConfirmation.includes("CONFIRM")){
                    var request = new XMLHttpRequest();
                    request.withCredentials=true;
                    request.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            // Typical action to be performed when the document is ready:
                            if(this.response.includes("success")){
                                window.location.href="clientTransaction.html?id="+clientId;
                            }
                            else{
                                alert("sorry, try again");
                            }
                            if(this.response.includes("LoggedOut")){
                                window.location.href="index.html";
                            }
                        }
                    };
                    request.open("POST",host+"/deleteClientTransaction",true);
                    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    var idData = {};
                    idData.id=id;
                    request.send(JSON.stringify(idData));
                }


            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}
function addTableRows(id,Date,Description,Debit,Credit,PaymentMode,bank,remarks){
    if(id==-1){
        var row = "<tr>";
        row+="<td >"+0+"</td>";
        row+="<td>"+Date+"</td>";
        row+="<td>"+Description+"</td>";
        row+="<td>"+PaymentMode+"</td>";
        row+="<td>"+Debit+"</td>";
        row+="<td>"+Credit+"</td>";
        row+="<td></td>"
        row+="</tr>";
        $('#transactionTable').append(row);
        balance+=Credit;
        balance-=Debit;
        balanceCredit+=Credit;
        balanceDebit-=Debit;
    }
    else if(id==-2){
        var row = "<tr>";
        row+="<td >"+'Total'+"</td>";
        row+="<td>"+Date+"</td>";
        row+="<td>"+Description+"</td>";
        row+="<td>"+PaymentMode+"</td>";
        row+="<td>"+Debit+"</td>";
        row+="<td>"+Credit+"</td>";
        row+="<td></td>"
        row+="</tr>";
        $('#transactionTable').append(row);
        rowCounter++;
    }
    else {
        var transaction = {};
        transaction.id=id;
        transaction.date=Date;
        transaction.description=Description;
        transaction.debit=Debit;
        transaction.credit=Credit;
        transaction.paymentMode=PaymentMode;
        transaction.bank=bank;
        transaction.remarks=remarks;
        transactions.push(transaction);
        var buttons = "<td><button type='button' class='btn btn-primary' onclick='edit(" + id + ")'><span class='glyphicon glyphicon-pencil'></span></button>";
        buttons += "<button type='button' class='btn btn-danger' onclick='deleteClient("+id+")'><span class='glyphicon glyphicon-trash'></span></button></td>";

        var row = "<tr>";
        row+="<td id ='"+id+"'>"+rowCounter+"</td>";
        row+="<td>"+Date+"</td>";
        row+="<td>"+Description+"</td>";
        row+="<td>"+PaymentMode+"</td>";
        if(Debit>0){
            row+="<td>-"+Debit+"</td>";
        }
        else{
            row+="<td>"+Debit+"</td>";
        }

        row+="<td>"+Credit+"</td>";
        row+=buttons;
        row+="</tr>";
        credits+=parseInt(Credit);
        balanceCredit+=parseInt(Credit);
        balanceDebit-=parseInt(Debit);
        debits+=parseInt(Debit);
        total+=(Credit-Debit);
        balance+=(Credit-Debit);
        $('#Credits').empty();
        $('#Credits').append(credits);
        $('#Debits').empty();
        $('#Debits').append(debits);
        var totalElement= document.getElementById('Total');
        if(total==0){
            totalElement.style.color="grey";
        }
        else if(total>0){
            totalElement.style.color="green";
        }
        else{
            totalElement.style.color="red";
        }
        $('#Total').empty();
        $('#Total').append(total);
        $('#transactionTable').append(row);

        rowCounter++;
    }

}

function initialConfiguration() {
    var clientId = getUrlParam("id",-1);
    var data = {};
    data.businessId = BusinessId;
    var bankRequest = new XMLHttpRequest();
    bankRequest.withCredentials=true;
    bankRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            var banks = JSON.parse(this.response);
            var bankList = document.getElementById("bankName");


            var newOption = document.createElement("OPTION");
            newOption.innerHTML="Select bank name";
            newOption.value=0;
            bankList.append(newOption);


            for(var key in banks){
                var bank = banks[key];
                var option = new Option(bank.name,bank.name);
                bankList.append(option);
                //editableBankList.appendChild(option);
            }
            var editableBankList = document.getElementById("editBankName");
            var newOption = document.createElement("OPTION");
            newOption.innerHTML="Select bank name";
            newOption.value=0;
            editableBankList.appendChild(newOption);
            for(var key in banks){
                var bank = banks[key];
                var option = new Option(bank.name,bank.name);
                editableBankList.appendChild(option);
            }
        }
    };
    bankRequest.open("POST",host+"/fetchBanks",true);
    bankRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    bankRequest.send(JSON.stringify(data));
    var paymentRequest = new XMLHttpRequest();
    paymentRequest.withCredentials=true;
    paymentRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            var payment = JSON.parse(this.response);
            var paymentModes = document.getElementById("paymentMode");

            var newOption = new Option('Select payment mode','0');
            paymentModes.appendChild(newOption);

            for(var key in payment){
                var payMode = payment[key];
                var option = new Option(payMode.mode,payMode.mode);
                paymentModes.appendChild(option);

            }
            var editablePaymentMode = document.getElementById("editPaymentMode");
            var newOption = new Option('Select payment mode','0');
            editablePaymentMode.appendChild(newOption);
            for(var key in payment){
                var payMode = payment[key];
                var option = new Option(payMode.mode,payMode.mode);

                editablePaymentMode.appendChild(option);
            }
        }
    };
    paymentRequest.open("POST",host+"/fetchPaymentMode",true);
    paymentRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    paymentRequest.send(JSON.stringify(data));
    var clientRequestData = {};
    clientRequestData.clientId = clientId;
    var clientRequest = new XMLHttpRequest();
    clientRequest.withCredentials=true;
    clientRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            var client = JSON.parse(this.response);

            var clientInfo = client[0][0];
            var transactions = client[1];
            var title = document.getElementById("clientName");
            if(clientInfo.openingBalance>0){
                addTableRows(-1,clientInfo.date,"Opening Balance",0,clientInfo.openingBalance,"","","");
            }
            else{
                addTableRows(-1,clientInfo.date,"Opening Balance",Math.abs(clientInfo.openingBalance),0,"","","");
            }

            title.innerText=clientInfo.name;
            var innerTitle  = document.getElementById("transactionClientName");
            innerTitle.innerText=clientInfo.name;
            var editTitle = document.getElementById("clientEditName");
            editTitle.innerText=clientInfo.name;
                for(var transactionKey in transactions){
                    if(transactions[transactionKey].transactionmode.includes("credit")){
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,transactions[transactionKey].description,0,transactions[transactionKey].amount,transactions[transactionKey].paymentmode,transactions[transactionKey].bankname,transactions[transactionKey].extra);
                    }
                    else{
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,transactions[transactionKey].description,transactions[transactionKey].amount,0,transactions[transactionKey].paymentmode,transactions[transactionKey].bankname,transactions[transactionKey].extra);
                    }


                }
            addTableRows(-2,"","Sub Total :",balanceDebit,balanceCredit,"");
            if(balance>=0){
                addTableRows(-2,"","Grand Total :",0,balance,"","","");
            }
            else{
                addTableRows(-2,"","Grand Total :",balance,0,"","","");
            }
            var table=$("#clientTransactions").DataTable({
                dom:"Blfrtip",
                buttons:[
                    {extend:"copy",className:"btn-sm"},
                    {extend:"csv",className:"btn-sm"},
                    {extend:"excel",className:"btn-sm"},
                    {
                        text: 'Pdf',
                        className:"btn-sm",
                        extend: 'pdfHtml5',
                        filename: businessName,
                        orientation: 'portrait', //portrait
                        pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                        exportOptions: {
                            columns: [0,1,2,3,4,5],
                            search: 'applied',
                            order: 'applied'
                        },
                        customize: function (doc) {
                            var now = new Date();
                            var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                            doc.content[1].table.body[0][0].align="left";

                            var objLayout = {};
                            objLayout['hLineWidth'] = function(i) { return .5; };
                            objLayout['vLineWidth'] = function(i) { return .5; };
                            objLayout['hLineColor'] = function(i) { return '#000000'; };
                            objLayout['vLineColor'] = function(i) { return '#000000'; };
                            objLayout['paddingLeft'] = function(i) { return 4; };
                            objLayout['paddingRight'] = function(i) { return 4; };
                            doc.content[1].layout = objLayout;

                            doc['header']=(function() {
                                return {
                                    columns: [
                                        {
                                            alignment: 'left',
                                            fontSize: 14,
                                            text: 'Business Name: '+businessName
                                        },
                                        {
                                          alignment: 'center',
                                          fontSize: 14,
                                          text:'Client Name: '+clientInfo.name
                                        },
                                        {
                                            alignment: 'right',
                                            fontSize: 14,
                                            text: 'Date: '+jsDate
                                        }
                                    ],
                                    margin: 20
                                }
                            });
                        }
                    },
                    {extend:"print",className:"btn-sm"}]
            });
            table.buttons().container()
                .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        }
    };
    clientRequest.open("POST",host+"/fetchClientTransaction",true);
    clientRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    clientRequest.send(JSON.stringify(clientRequestData));


}

function addRangeDate(button){

    document.getElementById("dateRangeFilter").style.display='';

    $('filterCustomization').append(button);
    document.getElementById("dateFilter").placeholder="From";
}
function addFilterDate(button) {
    document.getElementById('plainFilter').style.display='';



    $('filterCustomization').append(button);
}

function reportFilter() {
    $('filterCustomization').empty();
    document.getElementById('plainFilter').style.display='none';
    document.getElementById("dateRangeFilter").style.display='none';
    var selectBox = document.getElementById("reportType");

    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    if(selectedValue.includes("DateFilter")){
        var button = '<div class="col-sm-2 col-md-2"><button class="btn btn-primary" onclick="dateRangeFilter()">Filter</button></div>';
        addRangeDate(button);
    }
    else if(selectedValue.includes("Week")){
        var button = '<div class="col-sm-2 col-md-2"><button class="btn btn-primary" onclick="weekFilter()">Filter</button></div>';
        addFilterDate(button);
    }
    else if(selectedValue.includes("Month")){
        var button = '<div class="col-sm-2 col-md-2"><button class="btn btn-primary" onclick="monthFilter()">Filter</button></div>';
        addFilterDate(button);
    }

    else if(selectedValue.includes("Year")){
        var button = '<div class="col-sm-2 col-md-2"><button class="btn btn-primary" onclick="yearFilter()">Filter</button></div>';
        addFilterDate(button);
    }
}


function weekFilter() {
    credits=0;
    debits=0;
    total=0;
    rowCounter=1;
    balance=0;
    balanceDebit=0;
    balanceCredit=0;
    var sendData = {};
    var date=document.getElementById("dateFilter").value;
    var dateArr = date.split(".");
    var inputDate = new Date(parseInt(dateArr[2]),parseInt(dateArr[1])-1,parseInt(dateArr[0]));
    sendData.week=inputDate.getWeek();
    sendData.year=dateArr[2];
    sendData.date=date;
    sendData.clientId=clientId;
    var filterRequest = new XMLHttpRequest();
    filterRequest.withCredentials=true;
    filterRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("Reload")) {
                alert("No Records found");
                window.location.href = 'clientTransaction.html?id=' + clientId;
            }
            else {
                var clientData = JSON.parse(this.response);
                var clientInfo = clientData.clientInfo;
                var transactions = clientData.transactions;

                $('#clientTransactions').DataTable().destroy();
                $('#transactionTable').empty();
                if (clientData.openingBalance > 0) {
                    addTableRows(-1, clientInfo.date, "Opening Balance", 0, clientData.openingBalance, "", "", "");
                } else {
                    addTableRows(-1, clientInfo.date, "Opening Balance", Math.abs(clientData.openingBalance), 0, "", "", "");
                }
                for (var transactionKey in transactions) {
                    if (transactions[transactionKey].transactionmode.includes("credit")) {
                        addTableRows(transactions[transactionKey].id, transactions[transactionKey].date, transactions[transactionKey].description, 0, transactions[transactionKey].amount, transactions[transactionKey].paymentmode, transactions[transactionKey].bankname, transactions[transactionKey].extra);
                    } else {
                        addTableRows(transactions[transactionKey].id, transactions[transactionKey].date, transactions[transactionKey].description, transactions[transactionKey].amount, 0, transactions[transactionKey].paymentmode, transactions[transactionKey].bankname, transactions[transactionKey].extra);
                    }


                }
                addTableRows(-2, "", "Sub Total :", balanceDebit, balanceCredit, "");
                if (balance >= 0) {
                    addTableRows(-2, "", "Grand Total :", 0, balance, "", "", "");
                } else {
                    addTableRows(-2, "", "Grand Total :", balance, 0, "", "", "");
                }
                var table = $("#clientTransactions").DataTable({
                    dom: "Blfrtip",
                    buttons: [
                        {extend: "copy", className: "btn-sm"},
                        {extend: "csv", className: "btn-sm"},
                        {extend: "excel", className: "btn-sm"},
                        {
                            text: 'Pdf',
                            className: "btn-sm",
                            extend: 'pdfHtml5',
                            filename: businessName,
                            orientation: 'portrait', //portrait
                            pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5],
                                search: 'applied',
                                order: 'applied'
                            },
                            customize: function (doc) {
                                var now = new Date();
                                var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                                doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                doc.content[1].table.body[0][0].align = "left";

                                var objLayout = {};
                                objLayout['hLineWidth'] = function (i) {
                                    return .5;
                                };
                                objLayout['vLineWidth'] = function (i) {
                                    return .5;
                                };
                                objLayout['hLineColor'] = function (i) {
                                    return '#000000';
                                };
                                objLayout['vLineColor'] = function (i) {
                                    return '#000000';
                                };
                                objLayout['paddingLeft'] = function (i) {
                                    return 4;
                                };
                                objLayout['paddingRight'] = function (i) {
                                    return 4;
                                };
                                doc.content[1].layout = objLayout;

                                doc['header'] = (function () {
                                    return {
                                        columns: [
                                            {
                                                alignment: 'left',
                                                fontSize: 14,
                                                text: 'Business Name: ' + businessName
                                            },
                                            {
                                                alignment: 'center',
                                                fontSize: 14,
                                                text: 'Client Name: ' + clientInfo.name
                                            },
                                            {
                                                alignment: 'right',
                                                fontSize: 14,
                                                text: 'Date: ' + jsDate
                                            }
                                        ],
                                        margin: 20
                                    }
                                });
                            }
                        },
                        {extend: "print", className: "btn-sm"}]
                });
                table.buttons().container()
                    .appendTo('#example_wrapper .col-md-6:eq(0)');

            }

        }
    };
    filterRequest.open("POST",host+"/fetchWeekBalance",true);
    filterRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    filterRequest.send(JSON.stringify(sendData));

}

function monthFilter(){
    credits=0;
    debits=0;
    total=0;
    rowCounter=1;
    balance=0;
    balanceDebit=0;
    balanceCredit=0;
    var sendData = {};
    var date=document.getElementById("dateFilter").value;
    var dateArr = date.split(".");
    sendData.month = dateArr[1];
    sendData.year=dateArr[2];
    sendData.date=date;
    sendData.clientId=clientId;
    var filterRequest = new XMLHttpRequest();
    filterRequest.withCredentials=true;
    filterRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("Reload")){
                alert("No Records found");
                window.location.href='clientTransaction.html?id='+clientId;
            }
            else{
                var clientData = JSON.parse(this.response);
                var clientInfo = clientData.clientInfo;
                var transactions = clientData.transactions;

                $('#clientTransactions').DataTable().destroy();
                $('#transactionTable').empty();
                if(clientData.openingBalance>0){
                    addTableRows(-1,clientInfo.date,"Opening Balance",0,clientData.openingBalance,"","","");
                }
                else{
                    addTableRows(-1,clientInfo.date,"Opening Balance",Math.abs(clientData.openingBalance),0,"","","");
                }
                for(var transactionKey in transactions){
                    if(transactions[transactionKey].transactionmode.includes("credit")){
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,transactions[transactionKey].description,0,transactions[transactionKey].amount,transactions[transactionKey].paymentmode,transactions[transactionKey].bankname,transactions[transactionKey].extra);
                    }
                    else{
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,transactions[transactionKey].description,transactions[transactionKey].amount,0,transactions[transactionKey].paymentmode,transactions[transactionKey].bankname,transactions[transactionKey].extra);
                    }


                }
                addTableRows(-2,"","Sub Total :",balanceDebit,balanceCredit,"");
                if(balance>=0){
                    addTableRows(-2,"","Grand Total :",0,balance,"","","");
                }
                else{
                    addTableRows(-2,"","Grand Total :",balance,0,"","","");
                }
                var table=$("#clientTransactions").DataTable({
                    dom:"Blfrtip",
                    buttons:[
                        {extend:"copy",className:"btn-sm"},
                        {extend:"csv",className:"btn-sm"},
                        {extend:"excel",className:"btn-sm"},
                        {
                            text: 'Pdf',
                            className:"btn-sm",
                            extend: 'pdfHtml5',
                            filename: businessName,
                            orientation: 'portrait', //portrait
                            pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                            exportOptions: {
                                columns: [0,1,2,3,4,5],
                                search: 'applied',
                                order: 'applied'
                            },
                            customize: function (doc) {
                                var now = new Date();
                                var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                                doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                doc.content[1].table.body[0][0].align="left";

                                var objLayout = {};
                                objLayout['hLineWidth'] = function(i) { return .5; };
                                objLayout['vLineWidth'] = function(i) { return .5; };
                                objLayout['hLineColor'] = function(i) { return '#000000'; };
                                objLayout['vLineColor'] = function(i) { return '#000000'; };
                                objLayout['paddingLeft'] = function(i) { return 4; };
                                objLayout['paddingRight'] = function(i) { return 4; };
                                doc.content[1].layout = objLayout;

                                doc['header']=(function() {
                                    return {
                                        columns: [
                                            {
                                                alignment: 'left',
                                                fontSize: 14,
                                                text: 'Business Name: '+businessName
                                            },
                                            {
                                                alignment: 'center',
                                                fontSize: 14,
                                                text:'Client Name: '+clientInfo.name
                                            },
                                            {
                                                alignment: 'right',
                                                fontSize: 14,
                                                text: 'Date: '+jsDate
                                            }
                                        ],
                                        margin: 20
                                    }
                                });
                            }
                        },
                        {extend:"print",className:"btn-sm"}]
                });
                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
            }




        }
    };
    filterRequest.open("POST",host+"/fetchMonthBalance",true);
    filterRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    filterRequest.send(JSON.stringify(sendData));

}

function  yearFilter() {
    credits=0;
    debits=0;
    total=0;
    rowCounter=1;
    balance=0;
    balanceDebit=0;
    balanceCredit=0;
    var sendData = {};
    var date=document.getElementById("dateFilter").value;
    var dateArr = date.split(".");
    sendData.year=dateArr[2];
    sendData.clientId=clientId;
    sendData.date=date;
    var filterRequest = new XMLHttpRequest();
    filterRequest.withCredentials=true;
    filterRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("Reload")){
                alert("No Records found");
                window.location.href='clientTransaction.html?id='+clientId;
            }
            else{
                var clientData = JSON.parse(this.response);
                var clientInfo = clientData.clientInfo;
                var transactions = clientData.transactions;

                $('#clientTransactions').DataTable().destroy();
                $('#transactionTable').empty();
                if(clientData.openingBalance>0){
                    addTableRows(-1,clientInfo.date,"Opening Balance",0,clientData.openingBalance,"","","");
                }
                else{
                    addTableRows(-1,clientInfo.date,"Opening Balance",Math.abs(clientData.openingBalance),0,"","","");
                }
                for(var transactionKey in transactions){
                    if(transactions[transactionKey].transactionmode.includes("credit")){
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,transactions[transactionKey].description,0,transactions[transactionKey].amount,transactions[transactionKey].paymentmode,transactions[transactionKey].bankname,transactions[transactionKey].extra);
                    }
                    else{
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,transactions[transactionKey].description,transactions[transactionKey].amount,0,transactions[transactionKey].paymentmode,transactions[transactionKey].bankname,transactions[transactionKey].extra);
                    }


                }
                addTableRows(-2,"","Sub Total :",balanceDebit,balanceCredit,"");
                if(balance>=0){
                    addTableRows(-2,"","Grand Total :",0,balance,"","","");
                }
                else{
                    addTableRows(-2,"","Grand Total :",balance,0,"","","");
                }
                var table=$("#clientTransactions").DataTable({
                    dom:"Blfrtip",
                    buttons:[
                        {extend:"copy",className:"btn-sm"},
                        {extend:"csv",className:"btn-sm"},
                        {extend:"excel",className:"btn-sm"},
                        {
                            text: 'Pdf',
                            className:"btn-sm",
                            extend: 'pdfHtml5',
                            filename: businessName,
                            orientation: 'portrait', //portrait
                            pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                            exportOptions: {
                                columns: [0,1,2,3,4,5],
                                search: 'applied',
                                order: 'applied'
                            },
                            customize: function (doc) {
                                var now = new Date();
                                var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                                doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                doc.content[1].table.body[0][0].align="left";

                                var objLayout = {};
                                objLayout['hLineWidth'] = function(i) { return .5; };
                                objLayout['vLineWidth'] = function(i) { return .5; };
                                objLayout['hLineColor'] = function(i) { return '#000000'; };
                                objLayout['vLineColor'] = function(i) { return '#000000'; };
                                objLayout['paddingLeft'] = function(i) { return 4; };
                                objLayout['paddingRight'] = function(i) { return 4; };
                                doc.content[1].layout = objLayout;

                                doc['header']=(function() {
                                    return {
                                        columns: [
                                            {
                                                alignment: 'left',
                                                fontSize: 14,
                                                text: 'Business Name: '+businessName
                                            },
                                            {
                                                alignment: 'center',
                                                fontSize: 14,
                                                text:'Client Name: '+clientInfo.name
                                            },
                                            {
                                                alignment: 'right',
                                                fontSize: 14,
                                                text: 'Date: '+jsDate
                                            }
                                        ],
                                        margin: 20
                                    }
                                });
                            }
                        },
                        {extend:"print",className:"btn-sm"}]
                });
                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
            }




        }
    };
    filterRequest.open("POST",host+"/fetchYearBalance",true);
    filterRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    filterRequest.send(JSON.stringify(sendData));


}

function dateRangeFilter() {
    credits=0;
    debits=0;
    total=0;
    rowCounter=1;
    balance=0;
    balanceDebit=0;
    balanceCredit=0;
    var sendData = {};
    var fromDate=document.getElementById("dateFilterFrom").value;
    sendData.from=fromDate;
    var toDate = document.getElementById("dateFilterTo").value;
    sendData.to = toDate;
    sendData.clientId = clientId;
    var filterRequest = new XMLHttpRequest();
    filterRequest.withCredentials=true;
    filterRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("Reload")){
                alert("No Records found");
                window.location.href='clientTransaction.html?id='+clientId;
            }
            else{
                var clientData = JSON.parse(this.response);
                var clientInfo = clientData.clientInfo;
                var transactions = clientData.transactions;

                $('#clientTransactions').DataTable().destroy();
                $('#transactionTable').empty();

                for(var transactionKey in transactions){
                    if(transactions[transactionKey].transactionmode.includes("credit")){
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,transactions[transactionKey].description,0,transactions[transactionKey].amount,transactions[transactionKey].paymentmode,transactions[transactionKey].bankname,transactions[transactionKey].extra);
                    }
                    else{
                        addTableRows(transactions[transactionKey].id,transactions[transactionKey].date,transactions[transactionKey].description,transactions[transactionKey].amount,0,transactions[transactionKey].paymentmode,transactions[transactionKey].bankname,transactions[transactionKey].extra);
                    }


                }
                addTableRows(-2,"","Sub Total :",balanceDebit,balanceCredit,"");
                if(balance>=0){
                    addTableRows(-2,"","Grand Total :",0,balance,"","","");
                }
                else{
                    addTableRows(-2,"","Grand Total :",balance,0,"","","");
                }
                var table=$("#clientTransactions").DataTable({
                    dom:"Blfrtip",
                    buttons:[
                        {extend:"copy",className:"btn-sm"},
                        {extend:"csv",className:"btn-sm"},
                        {extend:"excel",className:"btn-sm"},
                        {
                            text: 'Pdf',
                            className:"btn-sm",
                            extend: 'pdfHtml5',
                            filename: businessName,
                            orientation: 'portrait', //portrait
                            pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                            exportOptions: {
                                columns: [0,1,2,3,4,5],
                                search: 'applied',
                                order: 'applied'
                            },
                            customize: function (doc) {
                                var now = new Date();
                                var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                                doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                                doc.content[1].table.body[0][0].align="left";

                                var objLayout = {};
                                objLayout['hLineWidth'] = function(i) { return .5; };
                                objLayout['vLineWidth'] = function(i) { return .5; };
                                objLayout['hLineColor'] = function(i) { return '#000000'; };
                                objLayout['vLineColor'] = function(i) { return '#000000'; };
                                objLayout['paddingLeft'] = function(i) { return 4; };
                                objLayout['paddingRight'] = function(i) { return 4; };
                                doc.content[1].layout = objLayout;

                                doc['header']=(function() {
                                    return {
                                        columns: [
                                            {
                                                alignment: 'left',
                                                fontSize: 14,
                                                text: 'Business Name: '+businessName
                                            },
                                            {
                                                alignment: 'center',
                                                fontSize: 14,
                                                text:'Client Name: '+clientInfo.name
                                            },
                                            {
                                                alignment: 'right',
                                                fontSize: 14,
                                                text: 'Date: '+jsDate
                                            }
                                        ],
                                        margin: 20
                                    }
                                });
                            }
                        },
                        {extend:"print",className:"btn-sm"}]
                });
                table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );
            }




        }
    };
    filterRequest.open("POST",host+"/fetchDateRangeTransaction",true);
    filterRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    filterRequest.send(JSON.stringify(sendData));

    console.log(sendData);
}