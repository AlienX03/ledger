function initialConfiguration(){
    var sessionId = getUrlParam("id",-1);
    if(sessionId!=-1){
        var updatedData = {};
        updatedData.session=sessionId;

        var request = new XMLHttpRequest();
        request.withCredentials=true;
        request.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if(!this.response.includes("success")){
                    var response = JSON.parse(this.response);
                    document.getElementById("mail").innerText=response.email;
                }
                else{
                    alert(this.response);
                }
            }
        };
        request.open("POST",host+"/requestPassword",true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(JSON.stringify(updatedData));

    }

}
initialConfiguration();

function changePassword(){
    var sessionId = getUrlParam("id",-1);
    if(sessionId!=-1){
        var password = document.getElementById("password").value;
        var confirmation = document.getElementById("confirm").value;
        var button = document.getElementById("submit");
        button.disabled=true;
        if(password.includes(confirmation)){
            var request = new XMLHttpRequest();
            var updatedData = {};
            updatedData.session=sessionId;
            updatedData.password=password;
            request.withCredentials=true;
            request.onreadystatechange = function() {

                if (this.readyState == 4 && this.status == 200) {
                    if(!this.response.includes("success")){
                       window.location.href="index.html";
                    }
                    else{
                        alert(this.response);
                    }
                }
            };
            request.open("POST",host+"/updatePassword",true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify(updatedData));

        }
        else{
            alert("Password doesnt matches");
            button.disabled=false;
        }
    }

}


function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getUrlParam(parameter, defaultvalue){
    var urlparameter = defaultvalue;

    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}
