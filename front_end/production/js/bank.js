var businessName = sessionStorage.getItem("businessName");
$("#headerTitle")[0].innerText= businessName;

var rowCounter=1;
var BusinessId = sessionStorage.getItem("businessId");
if (sessionStorage.getItem("businessId") === null) {
    alert("Sorry, no Records found");
} else {
    initialConfiguration()
}
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json", function (data) {
    host = data.host;

});
function initialConfiguration(){
    var request = new XMLHttpRequest();
    var data = {};
    data.businessId = BusinessId;
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            var banks = JSON.parse(this.response);
            for(var key in banks){
                var bank = banks[key];

                addCards(bank.id,bank.name,bank.businessId);
            }
            $('#editableTable').DataTable({});




        }
    };
    request.open("POST",host+"/fetchBanks",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(data));

}

var bankUser = [];

function addCards(id,BankName,BusinessId){
    var bank = {};
    bank.id=id;
    bank.name = BankName;
    bank.businessId = BusinessId;
    bankUser.push(bank);
    var buttons = "<td><button type='button' class='btn btn-primary' onclick='editUser("+id+")'><span class='glyphicon glyphicon-pencil'></span></button>";
    buttons+= "<button type='button' class='btn btn-danger' onclick='deleteBank("+id+")'><span class='glyphicon glyphicon-trash'></span></button></td>"
    var row = "<tr>";
    row+="<td id ='"+id+"'>"+rowCounter+"</td>";
    row+="<td>"+BankName+"</td>";
    row+=buttons;
    row+="</tr>";
    $('#Banks').append(row);

    rowCounter++;
}

function createUser(){
    var updatedData = {};
    updatedData.name = document.getElementById("newBankName").value;
    updatedData.businessId = BusinessId;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                location.reload();
            }
            else{
                alert("Sorry, please try again");
            }
        }
    };
    request.open("POST",host+"/addBank",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(updatedData));
}

function iterateUser(id){

    for(var itr=0;itr<bankUser.length;itr++){

        if(bankUser[itr].id==id){
            return bankUser[itr];
        }
    }
    return "Not found";
}

function deleteBank(id){
    $('.bs-delete-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-danger" onclick="deleteConfirmation('+id+')">Delete</button>';
    $('deleteButton').empty();
    $('deleteButton').append(button);
}

function editUser(id){
    var bank = iterateUser(id);
    if(typeof bank=="string"){
        alert("Failed");
    }
    else{
        $('.bs-edit-modal-sm').modal('toggle');
        var button = '<button type="button" class="btn btn-primary" onclick="editConfirmation('+id+')">Confirm</button>';
        $('editConfirmationButton').empty();
        $('editConfirmationButton').append(button);
    }
}
function editConfirmation(id){
    var username = document.getElementById("user").value;
    var password = document.getElementById("password").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                $('.bs-edit-modal-sm').modal('toggle')
                $('.bs-editBank-modal-sm').modal('toggle');
                var button = '<button type="button" class="btn btn-primary" onclick="update('+id+')">Update</button>';
                $('editButton').empty();
                $('editButton').append(button);
                var bank = iterateUser(id);
                document.getElementById("BankName").value=bank.name;

            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}
function  deleteConfirmation(id) {

    var username = document.getElementById("username").value;
    var password = document.getElementById("pass").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                var deleteConfirmation = prompt("Please enter CONFIRM to delete");
                if(deleteConfirmation.includes("CONFIRM")){
                    var request = new XMLHttpRequest();
                    request.withCredentials=true;

                    request.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            // Typical action to be performed when the document is ready:
                            if(this.response.includes("success")){
                                location.reload();
                            }
                            else{
                                alert("sorry, try again");
                            }
                            if(this.response.includes("LoggedOut")){
                                window.location.href="index.html";
                            }
                        }
                    };
                    request.open("POST",host+"/deleteBank",true);
                    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    var idData = {};
                    idData.id=id;
                    request.send(JSON.stringify(idData));
                }


            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}
function update(id) {
    var updatedData = {};
    updatedData.id = id;
    updatedData.name = document.getElementById("BankName").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                location.reload();
            }
            else{
                alert("Sorry, please try again");
            }
        }
    };
    request.open("POST",host+"/editBank",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(updatedData));
}

function validate(){
    if(document.getElementById("newBankName").value!="" && BusinessId!=""){
        createUser()
    }
    else{
        alert("Please fill all the particulars");
    }
}