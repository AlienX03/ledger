$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json", function (data) {
    host = data.host;

});
function forgetPassword() {
    var mailId = document.getElementById("InputEmail").value;
    var updatedData = {};
    updatedData.email=mailId;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("success")){
                alert("Check your mail for recovery");
            }
            else{
                alert(this.response);
            }
        }
    };
    request.open("POST",host+"/forgotPassword",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(updatedData));
}