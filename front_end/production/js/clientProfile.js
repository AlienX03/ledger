var businessName = sessionStorage.getItem("businessName");
$("#headerTitle")[0].innerText= businessName;

var rowCounter = 1;
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json",function (data) {
    host=data.host;

})


function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getUrlParam(parameter, defaultvalue){
    var urlparameter = defaultvalue;

    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}
var BusinessId = sessionStorage.getItem("businessId");
if (sessionStorage.getItem("businessId") === null) {
    alert("Sorry, no Records found");
} else {
    initialConfiguration()
}
function initialConfiguration(){

    var request = new XMLHttpRequest();
    var data = {};
    data.businessId = BusinessId;
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            var clients = JSON.parse(this.response);

            for(var client of clients){

                addBlocks(client.id,client.name,client.debits,client.credits);
            }
            $("#clientProfiles").DataTable({
                dom:"Blfrtip",
                buttons:[
                    {extend:"copy",className:"btn-sm"},
                    {extend:"csv",className:"btn-sm"},
                    {extend:"excel",className:"btn-sm"},
                    {
                        text: 'Pdf',
                        className:"btn-sm",
                        extend: 'pdfHtml5',
                        filename: businessName,
                        orientation: 'portrait', //portrait
                        pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                        exportOptions: {
                            columns: ':visible',
                            search: 'applied',
                            order: 'applied'
                        },
                        customize: function (doc) {
                            var now = new Date();
                            var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                            doc.content[1].table.body[0][0].align="left";

                            var objLayout = {};
                            objLayout['hLineWidth'] = function(i) { return .5; };
                            objLayout['vLineWidth'] = function(i) { return .5; };
                            objLayout['hLineColor'] = function(i) { return '#000000'; };
                            objLayout['vLineColor'] = function(i) { return '#000000'; };
                            objLayout['paddingLeft'] = function(i) { return 4; };
                            objLayout['paddingRight'] = function(i) { return 4; };
                            doc.content[1].layout = objLayout;

                            doc['header']=(function() {
                                return {
                                    columns: [
                                        {
                                            alignment: 'left',
                                            fontSize: 14,
                                            text: 'Business Name: '+businessName
                                        },
                                        {
                                            alignment: 'right',
                                            fontSize: 14,
                                            text: 'Date: '+jsDate
                                        }
                                    ],
                                    margin: 20
                                }
                            });
                        }
                    },
                    {extend:"print",className:"btn-sm"}]
            });





        }
    };
    request.open("POST",host+"/fetchClientsInfo",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(data));
}
/*
function addBlocks(id,clientName,credit,debit){
    var total = credit-debit;
    var rowBlock = "";
    rowBlock+='<div class="col-md-6 col-sm-6">';
        rowBlock+='<div class="x_panel">';
            rowBlock+='<div class="x_title"><button type="button" style="background: transparent;border: none !important;" onclick="viewClient('+id+')"><h4><strong>'+clientName+'</strong> </h4></button></div>';
            rowBlock+='<div class="x_content">';
                rowBlock+='<div class="row">';
                    rowBlock+='<div class="col-md-6 col-sm-6">';
                        rowBlock+='<div class="row">Credits</div>';
                        rowBlock+='<div class="row" style="color: green">'+credit+'</div>';
                    rowBlock+='</div>';
                    rowBlock+='<div class="col-md-6 col-sm-6 pull-right">';
                        rowBlock+='<div class="row" style="text-align: right;">Debits</div>';
                        rowBlock+='<div class="row" style="text-align: right;color: red">'+debit+'</div>';
                    rowBlock+='</div>';
                rowBlock+='</div>';
            rowBlock+='</div>';
            rowBlock+='<div class="x_content">';
                rowBlock+='<div class="row">';
                    rowBlock+='<div class="col-md-5 col-sm-3"></div>';
                    rowBlock+='<div class="col-md-2 col-sm-4">';
                        rowBlock+='<div class="row">Total</div>';
                        if(total==0){
                            rowBlock+='<div class="row">'+total+'</div>';
                        }
                        else if(total>0){
                            rowBlock+='<div class="row" style="color: green">'+total+'</div>';
                        }
                        else{
                            rowBlock+='<div class="row" style="color: red">'+total+'</div>';
                        }

                    rowBlock+='</div>';
                    rowBlock+='<div class="col-md-5 col-sm-3"></div>';
                rowBlock+='</div>';
            rowBlock+='</div>';
        rowBlock+='</div>';
    rowBlock+='</div>';
    if(iteratorCards%2==0){
        rowBlock+='<br>';
        console.log(id);
    }
    $('#ClientBlocks').append(rowBlock);
    iteratorCards++;
}
*/


function addBlocks(id,clientName,credit,debit){
    var total = credit+debit;
    var row = "<tr>";
    row+="<td id ='"+id+"'>"+rowCounter+"</td>";
    row+="<td><button type='button' style='background: transparent;border: none !important;' onclick='viewClient("+id+")'>"+clientName+"</button></td>";;
    row+="<td>"+credit+"</td>";
    row+="<td>"+debit+"</td>";
    row+="<td>"+total+"</td>";
    row+="</tr>";
 $('#transactionTable').append(row);

    rowCounter++;
}


function viewClient(id){
    window.location.href = "clientTransaction.html?id="+id;
}





