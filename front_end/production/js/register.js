var host="";
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json",function (data) {
    host=data.host;

})

function registerFunction(){
    var name = document.getElementById("FirstName").value+document.getElementById("LastName").value;
    var mail = document.getElementById("InputEmail").value;
    var password = document.getElementById("InputPassword").value;
    var gender = document.getElementById("Gender").value;
    var passwordConfirmation = document.getElementById("RepeatPassword").value;
    if(!password.includes(passwordConfirmation)){
        alert("Sorry, password didn't match");
    }
    else{
        var createUserRequest = new XMLHttpRequest();
        createUserRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                // Typical action to be performed when the document is ready:
                console.log(this.response);
                if(this.response.includes("success")){
                    alert("Account has been created");
                    window.location.href="index.html";
                }
            }
        };
        createUserRequest.open("POST",host+"/createUser",true);
        createUserRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        var jsonData = {};
        jsonData.mail = mail;
        jsonData.name = name;
        jsonData.gender = gender;
        jsonData.password = password;
        createUserRequest.send(JSON.stringify(jsonData));
    }
}