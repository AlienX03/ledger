var businessName = sessionStorage.getItem("businessName");
$("#headerTitle")[0].innerText= businessName;

var rowCounter = 1;
var BusinessId = sessionStorage.getItem("businessId");
if (sessionStorage.getItem("businessId") === null) {
    alert("Sorry, no Records found");
} else {
    initialConfiguration()
}
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json", function (data) {
    host = data.host;

});
var clientUser = [];

function addCards(id, clientName, address, mobile, mail, website, openingBalance,date) {
    var client = {};
    client.id = id;
    client.name = clientName;
    client.address = address;
    client.mobile = mobile;
    client.mail = mail;
    client.website = website;
    client.openingBalance = openingBalance;
    client.date=date;
    clientUser.push(client);
    var buttons = "<td><button type='button' class='btn btn-primary' onclick='edit(" + id + ")'><span class='glyphicon glyphicon-pencil'></span></button>";
    buttons += "<button type='button' class='btn btn-danger' onclick='deleteClient("+id+")'><span class='glyphicon glyphicon-trash'></span></button></td>";
    var row = "<tr>";
    row += "<td id ='" + id + "'>" + rowCounter + "</td>";
    row += "<td>" + clientName + "</td>";
    row += buttons;
    row += "</tr>";
    $('#Clients').append(row);

    rowCounter++;
}

function validate(){
    if(document.getElementById("newClientName").value!="" && document.getElementById("newAddress").value!="" && document.getElementById("newPhNO").value!="" && document.getElementById("newMail").value!="" && document.getElementById("newWebsite").value!="" &&  document.getElementById("newOpeningBalance").value!=""){
        createUser();

    }
    else{
        alert("Please fill all the particulars");
    }
}

function createUser() {
    var updatedData = {};
    updatedData.name = document.getElementById("newClientName").value;
    updatedData.businessId = BusinessId;
    updatedData.address = document.getElementById("newAddress").value;
    updatedData.mobile = document.getElementById("newPhNO").value;
    updatedData.email = document.getElementById("newMail").value;
    updatedData.website = document.getElementById("newWebsite").value;
    var balance=0;
    if(document.getElementById("credit").checked){
        balance = document.getElementById("newOpeningBalance").value;
    }
    else{
        balance-= document.getElementById("newOpeningBalance").value;
    }
    updatedData.openingBalance = balance;
    updatedData.date = document.getElementById("newDate").value;

    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                location.reload();
            }
            else{
                alert("Sorry, please try again");
            }
        }
    };
    request.open("POST",host+"/addClientsProfile",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(updatedData));
}

function iterateUser(id) {

    for (var itr = 0; itr < clientUser.length; itr++) {

        if (clientUser[itr].id == id) {
            return clientUser[itr];
        }
    }
    return "Not found";
}


function editUser(id) {
    var client = iterateUser(id);
    if (typeof client == "string") {
        alert("Failed");
    } else {
        $('.bs-editClient-modal-sm').modal('toggle');
        var button = '<button type="button" class="btn btn-primary" onclick="update(' + id + ')">Update</button>';
        $('editButton').empty();
        $('editButton').append(button);
        document.getElementById("ClientName").value = client.name;
        document.getElementById("Address").value = client.address;
        document.getElementById("mobile").value = client.mobile;
        document.getElementById("Mail").value = client.mail;
        document.getElementById("website").value = client.website;
        if(client.openingBalance>0){
            document.getElementById("editCredit").checked = true;
        }
        else{
            document.getElementById("editDebit").checked=true;
        }
        document.getElementById("openingBalance").value = Math.abs(client.openingBalance);
        document.getElementById("date").value = client.date;

    }
}

function update(id){
    var updatedData = {};
    updatedData.clientId = id;
    updatedData.name = document.getElementById("ClientName").value;
    updatedData.address = document.getElementById("Address").value;
    updatedData.mobile = document.getElementById("mobile").value;
    updatedData.email = document.getElementById("Mail").value;
    updatedData.website = document.getElementById("website").value;
    updatedData.openingBalance = document.getElementById("openingBalance").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                location.reload();
            }
            else{
                alert("Sorry, please try again");
            }
        }
    };
    request.open("POST",host+"/editClientsProfile",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(updatedData));
}


function initialConfiguration(){
    var request = new XMLHttpRequest();
    var data = {};
    data.businessId = BusinessId;
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            var clients = JSON.parse(this.response);
            for(var key in clients){
                var client = clients[key];
                addCards(client.id,client.name,client.address,client.mobile,client.email,client.website,client.openingBalance,client.date);
            }
            $('#editableTable').DataTable({});




        }
    };
    request.open("POST",host+"/fetchClientsProfile",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(data));

}

function deleteClient(id){
    $('.bs-delete-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-danger" onclick="deleteConfirmation('+id+')">Delete</button>';
    $('deleteButton').empty();
    $('deleteButton').append(button);
}
function edit(id){
    $('.bs-edit-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-primary" onclick="editConfirmation('+id+')">Confirm</button>';
    $('editConfirmationButton').empty();
    $('editConfirmationButton').append(button);
}


function editConfirmation(id){
    var username = document.getElementById("user").value;
    var password = document.getElementById("password").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            // Typical action to be performed when the document is ready:
            if(this.response.includes("success")){
                $('.bs-edit-modal-sm').modal('toggle')
                $('.bs-editUser-modal-sm').modal('toggle');
                editUser(id);
            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}
function  deleteConfirmation(id) {

    var username = document.getElementById("username").value;
    var password = document.getElementById("pass").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            // Typical action to be performed when the document is ready:
            if(this.response.includes("success")){
                var deleteConfirmation = prompt("Please enter CONFIRM to delete");
                if(deleteConfirmation.includes("CONFIRM")){
                    var request = new XMLHttpRequest();
                    request.withCredentials=true;

                    request.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            // Typical action to be performed when the document is ready:
                            if(this.response.includes("success")){
                                location.reload();
                            }
                            else{
                                alert("sorry, try again");
                            }
                            if(this.response.includes("LoggedOut")){
                                window.location.href="index.html";
                            }
                        }
                    };
                    request.open("POST",host+"/deleteClientsProfile",true);
                    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    var idData = {};
                    idData.clientId=id;
                    request.send(JSON.stringify(idData));
                }


            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}