var businessName = sessionStorage.getItem("businessName");
$("#headerTitle")[0].innerText= businessName;
var rowCounter=1;
var BusinessId = sessionStorage.getItem("businessId");
if (sessionStorage.getItem("businessId") === null) {
    alert("Sorry, no Records found");
} else {
    initialConfiguration()
}
$.ajaxSetup({
    async: false
});
$.getJSON("./js/config.json", function (data) {
    host = data.host;
});
var paymentUser = [];

function addCards(id,paymentName,businessId){
    var payment = {};
    payment.id=id;
    payment.name = paymentName;
    payment.businessId = businessId;
    paymentUser.push(payment);
    var buttons = "<td><button type='button' class='btn btn-primary' onclick='editUser("+id+")'><span class='glyphicon glyphicon-pencil'></span></button>";
    buttons+= "<button type='button' class='btn btn-danger' onclick='deletePayment("+id+")'><span class='glyphicon glyphicon-trash'></span></button></td>"
    var row = "<tr>";
    row+="<td id ='"+id+"'>"+rowCounter+"</td>";
    row+="<td>"+paymentName+"</td>";
    row+=buttons;
    row+="</tr>";
    $('#Payments').append(row);

    rowCounter++;
}

function editUser(id){
    var payment = iterateUser(id);
    if(typeof payment=="string"){
        alert("Failed");
    }
    else{
        $('.bs-edit-modal-sm').modal('toggle');
        var button = '<button type="button" class="btn btn-primary" onclick="editConfirmation('+id+')">Confirm</button>';
        $('editConfirmationButton').empty();
        $('editConfirmationButton').append(button);
    }
}
function editConfirmation(id){
    var username = document.getElementById("user").value;
    var password = document.getElementById("password").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            // Typical action to be performed when the document is ready:
            if(this.response.includes("success")){
                $('.bs-edit-modal-sm').modal('toggle')
                $('.bs-editPayment-modal-sm').modal('toggle');
                var button = '<button type="button" class="btn btn-primary" onclick="update('+id+')">Update</button>';
                $('editButton').empty();
                $('editButton').append(button);
                var payment = iterateUser(id);
                document.getElementById("PaymentMode").value=payment.name;

            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}
function update(id) {
    var updatedData = {};
    updatedData.id = id;
    updatedData.name = document.getElementById("PaymentMode").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if(this.response.includes("LoggedOut")){
            window.location.href="index.html";
        }
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("success")){
                location.reload();
            }
            else{
                alert("Sorry, please try again");
            }
        }
    };
    request.open("POST",host+"/editPaymentMode",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(updatedData));
}

function deletePayment(id){
    $('.bs-delete-modal-sm').modal('toggle');
    var button = '<button type="button" class="btn btn-danger" onclick="deleteConfirmation('+id+')">Delete</button>';
    $('deleteButton').empty();
    $('deleteButton').append(button);
}
function  deleteConfirmation(id) {

    var username = document.getElementById("username").value;
    var password = document.getElementById("pass").value;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            // Typical action to be performed when the document is ready:
            if(this.response.includes("success")){
                var deleteConfirmation = prompt("Please enter CONFIRM to delete");
                if(deleteConfirmation.includes("CONFIRM")){
                    var request = new XMLHttpRequest();
                    request.withCredentials=true;

                    request.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            // Typical action to be performed when the document is ready:
                            if(this.response.includes("success")){
                                location.reload();
                            }
                            else{
                                alert("sorry, try again");
                            }
                            if(this.response.includes("LoggedOut")){
                                window.location.href="index.html";
                            }
                        }
                    };
                    request.open("POST",host+"/deletePaymentMode",true);
                    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    var idData = {};
                    idData.id=id;
                    request.send(JSON.stringify(idData));
                }


            }
            else{
                alert("Sorry! no records found for the credentials");
            }
        }
    };
    request.open("POST",host+"/login",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var jsonData = {};
    jsonData.mail = username;
    jsonData.password = password;
    request.send(JSON.stringify(jsonData));
}

function initialConfiguration(){
    var request = new XMLHttpRequest();
    var data = {};
    data.businessId = BusinessId;
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            // Typical action to be performed when the document is ready:
            var payments = JSON.parse(this.response);
            for(var key in payments){
                var payment = payments[key];
                addCards(payment.id,payment.mode,payment.businessId);
            }
            $('#editableTable').DataTable({});
        }
    };
    request.open("POST",host+"/fetchPaymentMode",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(data));

}
function validate(){
    if(document.getElementById("newPaymentMode").value!="" && BusinessId!=""){
        createUser()
    }
    else{
        alert("Please fill all the particulars");
    }
}

function createUser(){
    var updatedData = {};
    updatedData.name = document.getElementById("newPaymentMode").value;
    updatedData.businessId = BusinessId;
    var request = new XMLHttpRequest();
    request.withCredentials=true;
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.response.includes("LoggedOut")){
                window.location.href="index.html";
            }
            if(this.response.includes("success")){
                location.reload();
            }
            else{
                alert("Sorry, please try again");
            }
        }
    };
    request.open("POST",host+"/addPaymentMode",true);
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify(updatedData));
}

function iterateUser(id){

    for(var itr=0;itr<paymentUser.length;itr++){

        if(paymentUser[itr].id==id){
            return paymentUser[itr];
        }
    }
    return "Not found";
}
