<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}
// Load Composer's autoloader
require 'vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
require_once __DIR__ . '/vendor/autoload.php';

    $htmlCode=$_POST['html'];
    $orders = $_POST['number'];
    $order  = str_replace('"','',$orders);
    $reciever = $_POST['reciever'];
    $message = $_POST['message'];

    $array = explode("_",$order);
    //$htmlCode = file_get_contents('123.html');
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);


    ini_set("pcre.backtrack_limit", "5000000");
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->list_indent_first_level = 0; 

    $stylesheet= file_get_contents('../customCSS/bootstrap.min.css');

    $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

    $mpdf->WriteHTML($htmlCode, \Mpdf\HTMLParserMode::HTML_BODY);

    $filename = "../tempLogs/".$order.".pdf";
    $mpdf->output($filename,'F');
    //Retrieving document
    ini_set('memory_limit', '-1');
    
    /*$dir = sys_get_temp_dir();
    $tmp = tempnam($dir, "foo.pdf");
    file_put_contents($tmp, $data);*/
	//$reciever = "j.ragul3333@gmail.com";
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'indigotrends07';                     // SMTP username
    $mail->Password   = 'trends123#';                               // SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->Port       = 587;                                    // TCP port to connect to
    $mail->AddAttachment($filename);         //Adds an attachment from a path on the filesystem

    //Recipients
    $mail->setFrom('indigotrends07@gmail.com', 'Indigo Trends');
    $mail->addAddress($reciever);     // Add a recipient
    //$mail->addAddress('ellen@example.com');               // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    // Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Purchase Order Reference - '.$order;
    $mail->Body    = "Dear sir/mam, Here we attached  ";
    if(strcmp($message,"undefined")!=0){
        $mail->Body = $message;
    }



    //$mail->    = "Purchase order Reference";
    $mail->AltBody = 'For further queries, contact- 9xxxxxxxx5';

    $mail->send();
    echo 'Message has been sent';

} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}