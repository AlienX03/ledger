var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer(); 
var session = require('express-session');
var cookieParser = require('cookie-parser');
var dbConfig = require("./dbConfig.json");
var properties = require("./config.json");
var allowedUser = require("./allowedUser.json");
var crypto = require('crypto');
var mysql = require('sync-mysql');

var nodemailer = require('nodemailer');
const { Console } = require('console');

var con = new mysql({
   host: dbConfig.host,
   user: dbConfig.user,
   password: dbConfig.password,
   database: dbConfig.schema
 });

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", properties.client); // update to match the domain you will make the request from
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      res.header("Access-Control-Allow-Credentials","true");
  
      res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  
        
   
   next();
 });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(upload.array());
app.use(cookieParser());
app.use(session({secret: "Your secret key"}));

function sendMail(reciever,Subject,Body,link){

var transporter = nodemailer.createTransport({
   service: 'gmail',
   auth: {
     user: properties.SMTP.mail,
     pass: properties.SMTP.password
   }
 });
   var mailOptions = {
      from: properties.SMTP.mail,
      to: reciever,
      subject: Subject,
      text: Body,
      html: '<p>Click <a href="'+link+'">here</a> to reset your password</p>'
    };
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        
      } else {
        return "Success";
      }
    });
}
function jsonConcat(o1, o2) {
   o1.transactions={};
   for (var key in o2) {
      
    o1.transactions[key] = o2[key];
   }
   return o1;
  }

function updateWeek(clientId,week,year,balance){
   var updateCheckWeekSql = "SELECT * FROM balanceweeks where clientId='"+clientId+"' and week='"+week+"' and year = '"+year+"';";
      var updateWeek = con.query(updateCheckWeekSql);
      if(updateWeek.length==0){
         var insertWeekSql = "INSERT INTO `balanceweeks` (`week`, `year`, `balance`, `clientId`) VALUES ('"+week+"', '"+year+"', '"+balance+"', '"+clientId+"');";
         var result = con.query(insertWeekSql);
      }
      else{
         var updatedBalance = parseInt(updateWeek[0].balance)+parseInt(balance);
         var updateWeekSql = "UPDATE `balanceweeks` SET `balance` = '"+updatedBalance+"' WHERE (`id` = '"+updateWeek[0].id+"');";
         
         var result = con.query(updateWeekSql);
      }
}
function updateMonth(clientId,month,year,balance){
   var updateCheckMonthSql = "SELECT * FROM balancemonth where clientId='"+clientId+"' and month='"+month+"' and year = '"+year+"';"
   var updateMonth = con.query(updateCheckMonthSql);
   if(updateMonth.length==0){
      var insertMonthSql = "INSERT INTO `balancemonth` (`month`, `year`, `balance`, `clientId`) VALUES ('"+month+"', '"+year+"', '"+balance+"', '"+clientId+"');";
      var result = con.query(insertMonthSql);
   }
   else{
      var updatedBalance = parseInt(updateMonth[0].balance)+parseInt(balance);
      var updateMonthSql = "UPDATE `balancemonth` SET `balance` = '"+updatedBalance+"' WHERE (`id` = '"+updateMonth[0].id+"');";
      var result = con.query(updateMonthSql);      
   }

}
function randomString(length, chars) {
   var result = '';
   for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
   return result;

}

app.post('/createUser',function(req,res){
   if(allowedUser.mail.includes(req.body.mail)){
      var checkSql = "select * from login where email ='"+req.body.mail+"';";
      var response="";
      var data = con.query(checkSql);
      
         if(data[0]!=undefined){
            response="userFound";
            res.send(response);
         }
         else{
            var mykey = crypto.createCipher('aes-128-cbc', 'mypassword');
            var password = mykey.update(req.body.password, 'utf8', 'hex');
            password += mykey.final('hex');
            var sql = "INSERT INTO `login` (`name`, `password`, `email`, `gender`) VALUES ('"+req.body.name+"', '"+password+"', '"+req.body.mail+"', '"+req.body.gender+"');";
            var result = con.query(sql);
            response="success";
            res.send(response);
         }
   }
   else{
      res.send("not allowed");
   }
    
});
app.post('/fetchBusinessSettings',(req,res)=>{
   if(req.session.user){
      var businessSql = "select * from businessprofile where id = '"+req.body.businessId+"'";
      var data = con.query(businessSql);
      res.send(JSON.stringify(data));
   }
   else{
      res.send("LoggedOut");
   }
});

app.post('/login', function(req, res){
   
   if(!req.body.mail || !req.body.password){
      res.send("Please enter both")
   } else {
      var mykey = crypto.createCipher('aes-128-cbc', 'mypassword');      
      var password = mykey.update(req.body.password, 'utf8', 'hex');
      password += mykey.final('hex');
      var sql = "select * from login where email='"+req.body.mail+"' and password='"+password+"';";
      var result = con.query(sql);      
         if(result[0]!=undefined){
            if(result[0].id!=undefined){
               response="success";
               req.session.user=req.body.mail;
            }
            else{
               response="failed";
            }
         }
         else{
            response="failed";
         }
         res.send(response);
   }
});



app.get('/fetchSession',(req,res) => {
   
    if(req.session.user){
        res.send("success");
    }
    else{
        res.send("LoggedOut")
    }
} );


app.get('/logout', function(req, res){
   req.session.destroy(function(){
      
      res.send("logging out");
   });
   
});

app.get('/fetchBusiness',function(req,res){
   if(req.session.user){
      var businessSql = "select * from businessprofile";
      var data = con.query(businessSql);
      res.send(JSON.stringify(data));
   }
   else{
      res.send("LoggedOut");
   }
});


app.post("/updateBusiness",function(req,res){
   if(req.session.user){
      var updateBusinessSql ="UPDATE `businessprofile` SET `name` = '"+req.body.name+"', `address` = '"+req.body.address+"', `mobile` = '"+req.body.mobile+"', `mail` = '"+req.body.mail+"', `tax` = '"+req.body.tax+"', `ie` = '"+req.body.ie+"' WHERE (`id` = '"+req.body.id+"');";
      var data = con.query(updateBusinessSql);
      
         if(data.changedRows>0){
            res.send("success");
         }
         else{
            res.send("failed");
         }
      
   }
   else{
      res.send("LoggedOut")
   }
});


app.post("/addBusiness",function(req,res){
   if(req.session.user){
      var addBusinessSql = "INSERT INTO `businessprofile` (`name`, `address`, `mobile`, `mail`, `tax`, `ie`) VALUES ('"+req.body.name+"', '"+req.body.address+"', '"+req.body.mobile+"', '"+req.body.mail+"', '"+req.body.tax+"', '"+req.body.ie+"');";
      var result = con.query(addBusinessSql);
      
         if(result.affectedRows>0){
            res.send("success");
         }
         else{
            res.send("failed");
         }
      
   }
   else{
      res.send("LoggedOut");
   }
});

app.post("/deleteBusiness",function(req,res){
   if(req.session.user){
      var deleteBusinessSql = "DELETE FROM `businessprofile` WHERE (`id` = '"+req.body.id+"');";

      var result = con.query(deleteBusinessSql);
         if(result.affectedRows>0){
            res.send("success");
         }
         else{
            res.send("failed");
         }
   }
   else{
      res.send("LoggedOut");
   }
});

app.post("/fetchClients",function(req,res){
   if(req.session.user){
      var businessNameSql = "SELECT * FROM businessprofile where id ='"+req.body.businessId+"';";
      var businessName = con.query(businessNameSql);
      var fetchClientsSql = "SELECT * FROM client where businessId ='"+req.body.businessId+"' order by id desc;";
      var resultJSON = {};
      resultJSON.businessName=businessName[0].name;
      var result = con.query(fetchClientsSql);
      var itr=0;
      for(row of result){
         
         var fetchClientTransactionSql = "SELECT * FROM clienttransaction where clientid = '"+row.id+"';"
         var additionalRows = con.query(fetchClientTransactionSql);
         resultJSON[itr] = jsonConcat(row,additionalRows);
         itr++;
      }
      res.send(JSON.stringify(resultJSON));
   }
   else{
      res.send("LoggedOut");
   }   
});
app.post("/fetchClientsProfile",function(req,res){
   if(req.session.user){
      var fetchClientsSql = "SELECT * FROM client where businessId ='"+req.body.businessId+"';";
      var result = con.query(fetchClientsSql);
      res.send(JSON.stringify(result));
   }
   else{
      res.send("LoggedOut");
   }   
});

app.post("/addClientsProfile",function(req,res){
   if(req.session.user){
      var fetchClientsSql = "INSERT INTO `client` (`name`, `businessId`, `address`, `mobile`, `email`, `website`, `openingBalance`,`date`) VALUES ('"+req.body.name+"', '"+req.body.businessId+"', '"+req.body.address+"', '"+req.body.mobile+"', '"+req.body.email+"', '"+req.body.website+"', '"+req.body.openingBalance+"', '"+req.body.date+"');";
      var result = con.query(fetchClientsSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
   
});


app.post("/editClientsProfile",function(req,res){
   if(req.session.user){   
      var fetchClientsSql = "UPDATE `client` SET `name` = '"+req.body.name+"', `address` = '"+req.body.address+"', `mobile` = '"+req.body.mobile+"', `email` = '"+req.body.email+"', `website` = '"+req.body.website+"', `openingBalance` = '"+req.body.openingBalance+"' WHERE (`id` = '"+req.body.clientId+"');";
      var result = con.query(fetchClientsSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }   
   
});

app.post("/deleteClientsProfile",function(req,res){
   if(req.session.user){
      var fetchClientsSql = "SELECT * FROM clienttransaction where clientid='"+req.body.clientId+"';";
      var clientsTransactionResults = con.query(fetchClientsSql);
      
      for(var row of clientsTransactionResults){
         var deleteTransactionSql = "DELETE FROM `clienttransaction` WHERE (`id` = '"+row.id+"');"
         var result = con.query(deleteTransactionSql);
      }
      var fetchBalanceMonthSql = "SELECT * FROM balancemonth where clientId='"+req.body.clientId+"';"
      var balanceMonthResult = con.query(fetchBalanceMonthSql);
      for(var row of balanceMonthResult){
         var deleteBalanceSql = "DELETE FROM `balancemonth` WHERE (`id` = '"+row.id+"');";
         var result = con.query(deleteBalanceSql);
      }
      var fetchBalanceWeekSql = "SELECT * FROM balanceweeks where clientId='"+req.body.clientId+"';"
      var balanceWeekResult = con.query(fetchBalanceWeekSql);
      for(var row of balanceWeekResult){
         var deleteBalanceSql = "DELETE FROM `balanceweeks` WHERE (`id` = '"+row.id+"');";
         var result = con.query(deleteBalanceSql);
      }
      var deleteClientSql = "DELETE FROM `client` WHERE (`id` = '"+req.body.clientId+"');";
      var finalResult = con.query(deleteClientSql);
      if(finalResult.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
});
app.post("/fetchBanks",function(req,res){
   if(req.session.user){
      var fetchBanksSql = "SELECT * FROM banks where businessId ='"+req.body.businessId+"';";
      var result = con.query(fetchBanksSql);
      res.send(JSON.stringify(result));
   }
   else{
      res.send("LoggedOut");
   }
});

app.post("/addBank",function(req,res){
   if(req.session.user){
      var fetchBanksSql = "INSERT INTO `banks` (`name`, `businessId`) VALUES ('"+req.body.name+"', '"+req.body.businessId+"');";
      var result = con.query(fetchBanksSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
});
app.post("/editBank",function(req,res){
   if(req.session.user){
      var fetchBanksSql = "UPDATE `banks` SET `name` = '"+req.body.name+"' WHERE (`id` = '"+req.body.id+"');";
      var result = con.query(fetchBanksSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
});
app.post("/deleteBank",function(req,res){
   if(req.session.user){
      var fetchBanksSql = "DELETE FROM `banks` WHERE (`id` = '"+req.body.id+"');";
      var result = con.query(fetchBanksSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
});

app.post("/fetchPaymentMode",function(req,res){
   if(req.session.user){
      var fetchPaymentSql = "SELECT * FROM payment where businessId ='"+req.body.businessId+"';";
      var result = con.query(fetchPaymentSql);
      res.send(JSON.stringify(result));
   }
   else{
      res.send("LoggedOut");
   }
});

app.post("/addPaymentMode",function(req,res){
   if(req.session.user){
      var fetchPaymentSql = "INSERT INTO `payment` (`mode`, `businessId`) VALUES ('"+req.body.name+"', '"+req.body.businessId+"');";
      var result = con.query(fetchPaymentSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
});
app.post("/editPaymentMode",function(req,res){
   if(req.session.user){
      var fetchPaymentSql = "UPDATE `payment` SET `mode` = '"+req.body.name+"' WHERE (`id` = '"+req.body.id+"');";
      var result = con.query(fetchPaymentSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
});
app.post("/deletePaymentMode",function(req,res){
   if(req.session.user){
      var fetchPaymentSql = "DELETE FROM `payment` WHERE (`id` = '"+req.body.id+"');";
      var result = con.query(fetchPaymentSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
});


app.post("/fetchClientsInfo",(req,res)=>{
   if(req.session.user){
      var result = [];
      var clientProfileSql = "SELECT * FROM client where businessId = '"+req.body.businessId+"' order by id desc;";
      var clientsResult = con.query(clientProfileSql);
      for(var row of clientsResult){
         var transactionSql = "SELECT * FROM clienttransaction where clientid='"+row.id+"' order by id desc;";
         var transactionResult = con.query(transactionSql);
         var client = {};
         client.id=row.id;
         client.name=row.name;
         client.credits=0;
         client.debits=0;
         for(var transactions of transactionResult){
            if(transactions.transactionmode.includes("credit")){
               client.credits+=parseInt(transactions.amount);
            }
            else{
               client.debits-=parseInt(transactions.amount);
            }
         }
         result.push(client);
      }
      res.send(result);
      
   }
   else{
      res.send("LoggedOut");
   }
});

app.post("/addClientTransaction",(req,res)=>{
   if(req.session.user){
      var balance=0;
      if(req.body.mode.includes("credit")){
         balance=parseInt(req.body.balance);
      }
      else{
         balance-=parseInt(req.body.balance);
      }
      updateWeek(req.body.clientId,req.body.week,req.body.year,balance);
      updateMonth(req.body.clientId,req.body.month,req.body.year,balance);
      var insertTransactionSql = "INSERT INTO `clienttransaction` (`clientid`, `date`, `paymentmode`, `bankname`, `description`, `extra`, `amount`, `transactionmode`, `week`, `month`, `year`) VALUES ('"+req.body.clientId+"', '"+req.body.date+"', '"+req.body.paymentMode+"', '"+req.body.bank+"', '"+req.body.description+"', '"+req.body.remarks+"', '"+req.body.balance+"', '"+req.body.mode+"', '"+req.body.week+"', '"+req.body.month+"', '"+req.body.year+"');"
      var result = con.query(insertTransactionSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("LoggedOut");
   }
});
app.post("/editClientTransaction",(req,res)=>{
   if(req.session.user){
      var balance = 0;
      if(req.body.mode.includes("credit")){
         balance+=parseInt(req.body.balance);
      }
      else{
         balance-=parseInt(req.body.balance);
      }
      var fetchTransactionSql = "SELECT * FROM clienttransaction where id ='"+req.body.id+"';";
      var transactionResult = con.query(fetchTransactionSql);
      var previousAmount=0;
         if(transactionResult[0].transactionmode.includes("credit")){
            previousAmount+=transactionResult[0].amount;
         }
         else{
            previousAmount-=transactionResult[0].amount;
         }
      
         var updateOlderWeek = "SELECT * FROM balanceweeks where clientId='"+transactionResult[0].clientid+"' and week='"+transactionResult[0].week+"' and year = '"+transactionResult[0].year+"';";
         var olderWeekResult = con.query(updateOlderWeek);
         var updatedWeekBalance = parseInt(olderWeekResult[0].balance)-parseInt(previousAmount);
         var updateWeekSql = "UPDATE `balanceweeks` SET `balance` = '"+updatedWeekBalance+"' WHERE (`id` = '"+olderWeekResult[0].id+"');";
         var updateWeekResult = con.query(updateWeekSql);
         updateWeek(req.body.clientId,req.body.week,req.body.year,balance);
      
         var updateOlderMonth = "SELECT * FROM balancemonth where clientId='"+transactionResult[0].clientid+"' and month='"+transactionResult[0].month+"' and year = '"+transactionResult[0].year+"';";
         var olderMonthResult = con.query(updateOlderMonth);
         var updatedMonthBalance = parseInt(olderMonthResult[0].balance)-parseInt(previousAmount);
         var updateMonthSql = "UPDATE `balancemonth` SET `balance` = '"+updatedMonthBalance+"' WHERE (`id` = '"+olderMonthResult[0].id+"');";
         var updateMonthResult = con.query(updateMonthSql);
         updateMonth(req.body.clientId,req.body.month,req.body.year,balance);
   
      var updateTransactionSql = "UPDATE `clienttransaction` SET `date` = '"+req.body.date+"', `paymentmode` = '"+req.body.paymentMode+"', `bankname` = '"+req.body.bank+"', `description` = '"+req.body.description+"', `extra` = '"+req.body.remarks+"', `amount` = '"+req.body.balance+"', `transactionmode` = '"+req.body.mode+"', `week` = '"+req.body.week+"', `month` = '"+req.body.month+"', `year` = '"+req.body.year+"' WHERE (`id` = '"+req.body.id+"');"
      var result = con.query(updateTransactionSql);
      if(result.affectedRows>0){
         res.send("success");
      }
      else{
         res.send("failed");
      }   
   }
   else{
      res.send("LoggedOut");
   }
});

app.post("/deleteClientTransaction",(req,res)=>{
   if(req.session.user){
      var fetchTransactionSql = "SELECT * FROM clienttransaction where id ='"+req.body.id+"';";
      var transactionResult = con.query(fetchTransactionSql);
      var previousAmount=0;
         if(transactionResult[0].transactionmode.includes("credit")){
            previousAmount+=transactionResult[0].amount;
         }
         else{
            previousAmount-=transactionResult[0].amount;
         }
         var updateOlderWeek = "SELECT * FROM balanceweeks where clientId='"+transactionResult[0].clientid+"' and week='"+transactionResult[0].week+"' and year = '"+transactionResult[0].year+"';";
         var olderWeekResult = con.query(updateOlderWeek);
         var updatedWeekBalance = parseInt(olderWeekResult[0].balance)-parseInt(previousAmount);
         var updateWeekSql = "UPDATE `balanceweeks` SET `balance` = '"+updatedWeekBalance+"' WHERE (`id` = '"+olderWeekResult[0].id+"');";
         var updateWeekResult = con.query(updateWeekSql);
         
         
         var updateOlderMonth = "SELECT * FROM balancemonth where clientId='"+transactionResult[0].clientid+"' and month='"+transactionResult[0].month+"' and year = '"+transactionResult[0].year+"';";
         var olderMonthResult = con.query(updateOlderMonth);
         var updatedMonthBalance = parseInt(olderMonthResult[0].balance)-parseInt(previousAmount);
         var updateMonthSql = "UPDATE `balancemonth` SET `balance` = '"+updatedMonthBalance+"' WHERE (`id` = '"+olderMonthResult[0].id+"');";
         var updateMonthResult = con.query(updateMonthSql);
   
         var deleteTransaction = "DELETE FROM `clienttransaction` WHERE (`id` = '"+req.body.id+"');";
         var result = con.query(deleteTransaction);
         if(result.affectedRows>0){
            res.send("success");
         }
         else{
            res.send("failed");
         }
   
   }
   else{
      res.send("LoggedOut");
   }   

});

app.post("/fetchClientTransaction",(req,res)=>{
   if(req.session.user){
      var fetchClientSql = "SELECT * FROM client where id = '"+req.body.clientId+"' ;";
      var result = con.query(fetchClientSql);
      var fetchTransactionSql = "SELECT * FROM clienttransaction where clientid='"+req.body.clientId+"' order by id desc;";
      var transactions = con.query(fetchTransactionSql);
      var answer = [];
      answer[0]=result;
      answer[1]=transactions;
      res.send(answer);
   }
   else{
      res.send("LoggedOut");
   }
  
});


app.post("/fetchWeekBalance",(req,res)=>{
   if(req.session.user){
      var baseCheckSql="SELECT * FROM ledger.client where id='"+req.body.clientId+"' and str_to_date('"+req.body.date+"','%d.%m.%Y')>=str_to_date(date,'%d.%m.%Y');";
   var baseResult=con.query(baseCheckSql);
   if(baseResult.length>0){
      var openingBalanceSql = "SELECT * FROM client where id ='"+req.body.clientId+"'";
   var openingBalanceResult = con.query(openingBalanceSql);
   var weekBalanceSql  = "SELECT sum(balance) sum FROM balanceweeks where week<"+req.body.week+" and year<="+req.body.year+" and clientId="+req.body.clientId+";"
   var weekBalanceResult=con.query(weekBalanceSql);
   var result = {};
   result.clientInfo = openingBalanceResult[0];
   var weekBalance=0
   if(weekBalanceResult[0].sum!=null){
      
      weekBalance=weekBalanceResult[0].sum;
   }
   result.openingBalance = parseInt(openingBalanceResult[0].openingBalance)+parseInt(weekBalance);
   var transactionSql = "SELECT * FROM ledger.clienttransaction where week="+req.body.week+" and year="+req.body.year+" and clientid="+req.body.clientId+";";
   var transactionResult = con.query(transactionSql);
   
   result.transactions = transactionResult;
   res.send(result);
   }
   else{
      res.send("Reload");
   }
   
}
else{
   res.send("LoggedOut");
}

});
app.post("/fetchMonthBalance",(req,res)=>{
   if(req.session.user){
      var baseCheckSql="SELECT * FROM ledger.client where id='"+req.body.clientId+"' and str_to_date('"+req.body.date+"','%d.%m.%Y')>=str_to_date(date,'%d.%m.%Y');";
      var baseResult=con.query(baseCheckSql);
      
      if(baseResult.length>0){
         var openingBalanceSql = "SELECT * FROM client where id ='"+req.body.clientId+"'";
   var openingBalanceResult = con.query(openingBalanceSql);
   var monthBalanceSql  = "SELECT sum(balance) sum FROM balancemonth where month<"+req.body.month+" and year<="+req.body.year+" and clientId="+req.body.clientId+";"
   var monthBalanceResult=con.query(monthBalanceSql);
   var result = {};
   result.clientInfo = openingBalanceResult[0];
   var monthBalance=0
   if(monthBalanceResult[0].sum!=null){
      monthBalance=monthBalanceResult[0].sum;
   }
   result.openingBalance = parseInt(openingBalanceResult[0].openingBalance)+parseInt(monthBalance);
   var transactionSql = "SELECT * FROM ledger.clienttransaction where month="+req.body.month+" and year="+req.body.year+" and clientid="+req.body.clientId+";";
   var transactionResult = con.query(transactionSql);
   
   result.transactions = transactionResult;
   res.send(result);
      }
      else{
         res.send("Reload");
      }
   
}
else{
   res.send("LoggedOut");
}

});
app.post("/fetchYearBalance",(req,res)=>{
   
      if(req.session.user){
         var baseCheckSql="SELECT * FROM ledger.client where id='"+req.body.clientId+"' and str_to_date('"+req.body.date+"','%d.%m.%Y')>=str_to_date(date,'%d.%m.%Y');";
   var baseResult=con.query(baseCheckSql);
   if(baseResult.length>0){
         var openingBalanceSql = "SELECT * FROM client where id ='"+req.body.clientId+"'";
         var openingBalanceResult = con.query(openingBalanceSql);
         var yearBalanceSql  = "SELECT sum(balance) sum FROM balancemonth where  year<"+req.body.year+" and clientId="+req.body.clientId+";"
         var yearBalanceResult=con.query(yearBalanceSql);
         var result = {};
         result.clientInfo = openingBalanceResult[0];
         var yearBalance=0
         if(yearBalanceResult[0].sum!=null){
            yearBalance=yearBalanceResult[0].sum;
         }
         result.openingBalance = parseInt(openingBalanceResult[0].openingBalance)+parseInt(yearBalance);
         var transactionSql = "SELECT * FROM ledger.clienttransaction where year="+req.body.year+" and clientid="+req.body.clientId+";";
         var transactionResult = con.query(transactionSql);
         
         result.transactions = transactionResult;
         res.send(result);
   }
   else{
      res.send("Reload");
   }
   
}
else{
   res.send("LoggedOut");
}

});

app.post("/fetchDateRangeTransaction",(req,res)=>{
   if(req.session.user){
   var clientSql = "SELECT * FROM client where id ='"+req.body.clientId+"'";
   var clientResult = con.query(clientSql);
   var transactionSql = "SELECT * FROM clienttransaction where clientid='"+req.body.clientId+"' and str_to_date(date,'%d.%m.%Y') between STR_TO_DATE('"+req.body.from+"','%d.%m.%Y') and str_to_date('"+req.body.to+"','%d.%m.%Y') ;";
   
   var transactionResult = con.query(transactionSql);
   var result = {};
   result.clientInfo = clientResult[0];
   result.transactions = transactionResult;
   res.send(result);
}
else{
   res.send("LoggedOut");
}

});


app.post("/forgotPassword",(req,res)=>{
   var inputEmail = req.body.email;
   var checkMail = "SELECT * FROM login where email='"+inputEmail+"';";
   var result = con.query(checkMail);
   if(result.length>0){
      var string = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
      var updateSql = "UPDATE `ledger`.`login` SET `ran` = '"+string+"' WHERE (`id` = '"+result[0].id+"');";
      var updateResult = con.query(updateSql);
      var response = sendMail(inputEmail,"Forget Password Recovery - Reg.","Please click the below link to change the password",properties.client+"/passwordRecovery.html?id="+string);
      res.send("success");
   }
   else{
      res.send("Sorry no accounts found");
   }
});

app.post("/requestPassword",(req,res)=>{
   var inputSession = req.body.session;
   var fetchMail = "Select * from login where ran='"+inputSession+"'";
   var result = con.query(fetchMail);
   if(result.length>0){
      res.send(result[0]);
   }
   else{
      res.send("failed");
   }
})

app.post("/updatePassword" ,(req,res)=>{
   var fetchMail = "Select * from login where ran='"+req.body.session+"'";
   var result=con.query(fetchMail);
   if(result.length>0){
      var mykey = crypto.createCipher('aes-128-cbc', 'mypassword');
      var password = mykey.update(req.body.password, 'utf8', 'hex');
      password += mykey.final('hex');     
      var updatePasswordSql = "UPDATE `ledger`.`login` SET `password` = '"+password+"' WHERE (`id` = '"+result[0].id+"');";
      var updateResult = con.query(updatePasswordSql);
      if(updateResult.length>0){
         res.send("Success");
      }
      else{
         res.send("failed");
      }
   }
   else{
      res.send("failed");
   }
})
var port = properties.port || 3000;
app.listen(port,()=>{
    console.log("listening at "+port);
} );